#!/usr/bin/python3
'''
cassius
Copyright (C) 2019 Samuel First

    cassius is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cassius is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cassius.  If not, see <https://www.gnu.org/licenses/>.
'''

import argparse
import os
import sys

# Translate each card into ascii
def interpret(card):
    """Translate punch-codes to chars"""
    # Read lines from card
    lines = []
    with open(card) as c:
        for line in c:
            lines.append(line)

    # Get rid of human-readable part
    if config.discardFirstLine:
        lines.pop(0)

    # Read down each line in each column and
    # look for places where the card has been
    # punched.  If a space has been punched,
    # append its assosciated character to the
    # punch-code.  At the end of each column,
    # find the character assosciated with the
    # punch code, and append that character to
    # the interpreted line.  At the end of the
    # card, return the interpreted line.
    interpreted = ''
    for col in range(len(lines[0])):
        punchCode = ''
        modified = False
        for line in range(len(lines)):
            if lines[line][col] == ' ':
                if config.columns[line] == config.modifier:
                    modified = True
                else:
                    punchCode += config.columns[line]

            # If verbosity is enabled, dump variables
            if args.verbose:
                print('         col: ' + str(col) + '\n',
                      '       line: ' + str(line) + '\n',
                      '       char: ' + str(lines[line][col]) + '\n',
                      '  punchCode: ' + str(punchCode) + '\n',
                      '   modified: ' + str(modified) + '\n',
                      'interpreted: ' + str(interpreted) + '\n') 
                input("Press Enter to Continue")
                
        try:
            if modified and config.useModifier:
                interpreted += config.alternate[config.default[punchCode]]
            else:
                interpreted += config.default[punchCode]
        except KeyError:
            print('punch-code ' + punchCode +  ' not recognized')
            exit()

    return interpreted.rstrip()


# This runs only when called from
# the command line
if __name__ == '__main__':
    # Load/parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('dir',
                        type=str,
                        nargs='?',
                        help='directory to search for punch cards in',
                        default=os.getcwd())
    parser.add_argument('-l',
                        '--load',
                        type=str,
                        help='Specify alternate run control file',
                        default=None)
    parser.add_argument('-i',
                        '--interpreter',
                        type=str,
                        help='Specify interpreter to use',
                        default=None)
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true')
    parser.add_argument('-n',
                        '--no-run',
                        action='store_true',
                        help='Compile program, but don\'t run',
                        dest='noRun')
    args = parser.parse_args()

    # Load run control file (where
    # config options are specified)
    sys.dont_write_bytecode = True
    if args.load == None:
        sys.path.insert(0, os.path.expanduser('~/.config'))
        rcFile = 'cassiusrc'
    else:
        # Split args.load into (path, file)
        runControl = os.path.split(args.load)
        sys.path.insert(0, runControl[0])
        rcFile = os.pathsplitext(runControl[1])[0]
    try:
        config = __import__(rcFile)
    except:
        print('Could not load run control file')
        exit()

    # If interpreter is not set by flag, try
    # to use the one set in the run control
    # file
    if args.interpreter == None:
        try:
            args.interpreter = config.interpreter
        except AttributeError:
            print('No Interpreter Specified')
            exit()

    # Load punch cards and pass them to
    # the interpreter.  Then take each
    # interpreted line and append it to
    # a.out
    files = os.listdir(args.dir)
    files.sort()

    # 0 out a.out if it exists
    if os.path.isfile(args.dir + '/a.out'):
        with open(args.dir + '/a.out', 'w') as program:
            program.write('')
    
    for card in files:
        if card.isnumeric():
            if args.verbose:
                print('Reading Card ' + card)
            line = interpret(args.dir + '/' + card)
            with open(args.dir + '/a.out', 'a') as program:
                program.write(line + '\n')

    if not args.noRun:
        os.system(args.interpreter + ' ' + args.dir + '/a.out')
