# Cassius

Card punch emulator

## Index

* [What is Cassius?](https://gitlab.com/samuelfirst/cassius#what-is-cassius)
  * [Example of a Punched Card](https://gitlab.com/samuelfirst/cassius#example-of-a-punched-card)
* [How it works](https://gitlab.com/samuelfirst/cassius#how-it-works)
  * [How Punched Cards Work](https://gitlab.com/samuelfirst/cassius#how-punched-cards-work)
  * [How Cassius Works](https://gitlab.com/samuelfirst/cassius#how-cassius-works)
* [Installation](https://gitlab.com/samuelfirst/cassius#installation)
* [License](https://gitlab.com/samuelfirst/cassius#license)

## What is Cassius?
Cassius is a card punch emulator.  It translates the digital
equivalent of punched cards into valid code, and calls the
interpreter either specified in the run control file, or
with the --interpreter flag.

### Example of a Punched Card:

```
print("Hello World")
zzzzzzz zzzzz zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
yy yy y  yyyyyyyy yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
  x xxxxx   xx   xx xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
0000 00000000 000000000000000000000000000000000000000000000000000000000000000000
11111111111111111111111111111111111111111111111111111111111111111111111111111111
22222222222222222222222222222222222222222222222222222222222222222222222222222222
3333 3333  33333 333333333333333333333333333333333333333333333333333333333333333
44444444444444444 44444444444444444444444444444444444444444444444444444444444444
555 5 55 5555555555 555555555555555555555555555555555555555555555555555555555555
66666666666 6  66666666666666666666666666666666666666666666666666666666666666666
 77777 77777777777 7777777777777777777777777777777777777777777777777777777777777
88888   8888888888  888888888888888888888888888888888888888888888888888888888888
9  999999999999 9999999999999999999999999999999999999999999999999999999999999999
```

## How it Works

### How Punched Cards Work

Punched cards were encoded with a format called
[EBCDIC](https://en.wikipedia.org/wiki/EBCDIC), which allowed
each character to be represented by 1-3 values (y,x,0,1,2,3,4,5,6,7,8,9).
The code for each character was punched out on each card with
a machine called a [key punch](https://en.wikipedia.org/wiki/Keypunch).
The cards were then fed into a computer, and once the computer finished
interpreting them, it would give the output in the form of a printout.

Below is a table that can be used to determine the character assosciated
with each key-code.

|     | N/A   | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 2-8 | 3-8 | 4-8 | 5-8 | 6-8 | 7-8 |
|----:|:-----:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:---:|:---:|:---:|:---:|:---:|:---:|
| N/A | space | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | :   | #   | @   | '   | =   | "   |
|   Y | &     | A | B | C | D | E | F | G | H | I | [   | .   | <   | (   | +   | !   |
|   X | -     | J | K | L | M | N | O | P | Q | R | ]   | $   | *   | )   | ;   | ^   |
|   0 | 0     | / | S | T | U | V | W | X | Y | Z | \   | ,   | %   |     | >   | ?   |

### How Cassius Works

Cassius provides a slight modification to the traditional EBCDIC format,
by allowing for a modifier value (z) to be used to expand the default
character set to include both upper and lower case characters, as well as
a few other special characters.  This is so it can be used with more
modern programming languages, which require mixed-case.

The specifications for how to interpret the cards are loaded from the
run control file (~/.config/cassiusrc.py), and the card interpreter
reads through each card, and looks up the character assosciated with each
key-code.  It builds each line in this fashion, and appends them to the
file a.out.  After it has finished building the file, it shells out to
the specified interpreter, and runs the file.

## Installation

## License

Cassius is licensed under the GNU GPLv3, meaning you are free to
copy/modify/distribute all or part of it, so long as you
provide both a copy of the source and a copy of the GPL with
it or any derivative works.

More information can be found in COPYING
